﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tema2Service.Protos;
using Tema2Service;
using Microsoft.Extensions.Logging;
using Grpc.Net.Client;

namespace Tema2Service.Services
{
    public class ClientService : Client.ClientBase
    {
        private readonly ILogger<ClientService> _logger;
        public ClientService(ILogger<ClientService> logger)
        {
            _logger = logger;
        }
        public override Task<ClientReply> GetInfo(ClientRequest clientRequest, ServerCallContext context)
        {
            string m_date = clientRequest.Date;
            if(m_date[1].ToString()=="/")
            {
               string m_date1 = m_date.Insert(0, "0");
                m_date = m_date1;
                
            }
            if(m_date[4].ToString()=="/")
            {
                string m_date1;
                m_date1=m_date.Insert(3, "0");
                m_date = m_date1;
            }
            int month = Int32.Parse(m_date[0].ToString())*10+Int32.Parse(m_date[1].ToString());
            int day = Int32.Parse(m_date[3].ToString()) * 10 + Int32.Parse(m_date[4].ToString());

            //_logger.Log(LogLevel.Information, "\n" + clientRequest.Date + "\n");

            string zodiac="eroare";
            if (month >= 3 && month <= 5)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Spring.SpringClient(channel);
                var response = client.SpringInfo(new SpringRequest { Day = day, Month = month });
                Console.WriteLine(response);
                zodiac = response.ToString();
            }
            if (month >= 6 && month <= 8)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Summer.SummerClient(channel);
                var response = client.SummerInfo(new SummerRequest { Day = day, Month = month });
                Console.WriteLine(response);
                zodiac = response.ToString();
            }

            if (month >= 9 && month <= 11)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Autumn.AutumnClient(channel);
                var response = client.AutumnInfo(new AutumnRequest { Day = day, Month = month });
                Console.WriteLine(response);
                zodiac = response.ToString();
            }
            if (month == 11 || month == 12 || month == 1)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Winter.WinterClient(channel);
                var response = client.WinterInfo(new WinterRequest { Day = day, Month = month });
                Console.WriteLine(response);
                zodiac = response.ToString();
            }
            
            return Task.FromResult(new ClientReply { Reply = zodiac });

        }
    }
}
