﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tema2Service.Protos;
using Tema2Service;
using Microsoft.Extensions.Logging;
using Grpc.Core;
using System.IO;

namespace Tema2Service.Services
{
    public class WinterService : Winter.WinterBase
    {
        public override Task<WinterReply> WinterInfo(WinterRequest request, ServerCallContext context)
        {
            StreamReader citire_fisier = new StreamReader(@"FisiereText\Winter.txt");
            string zodiac = "eroare";
            string v_line;
            while ((v_line = citire_fisier.ReadLine()) != null)
            {
                int v_month_start = Int32.Parse(v_line[0].ToString()) * 10 + Int32.Parse(v_line[1].ToString());
                int v_day_start = Int32.Parse(v_line[3].ToString()) * 10 + Int32.Parse(v_line[4].ToString());
                int v_month_end = Int32.Parse(v_line[8].ToString()) * 10 + Int32.Parse(v_line[9].ToString());
                int v_day_end = Int32.Parse(v_line[11].ToString()) * 10 + Int32.Parse(v_line[12].ToString());
                int number_start = v_month_start * 100 + v_day_start;
                int number_end = v_month_end * 100 + v_day_end;
                int number = request.Month * 100 + request.Day;
                if (number >= number_start && number <= number_end)
                {
                    zodiac = v_line.Substring(14);
                    Console.WriteLine();
                    return Task.FromResult(new WinterReply { Reply = zodiac });
                }


            }


            return Task.FromResult(new WinterReply { Reply = zodiac });
        }
    }
}
