﻿using System;
using Tema2Service.Protos;
using Grpc.Net.Client;
using System.Threading.Tasks;

namespace Tema2Client
{
    class Program
    {
        public static bool IsDate(string date)
        {
            DateTime fromDateValue;
            var formats = new[] { "MM/dd/yyyy", "M/d/yyyy" };
            if (DateTime.TryParseExact(date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static async Task Main(string[] args)
        {
            string read_date;
            read_date = Console.ReadLine();
            if (IsDate(read_date)==true)
            {
                var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Client.ClientClient(channel);
                var response = await client.GetInfoAsync(new ClientRequest { Date = read_date });
                Console.WriteLine(response);
            }
            else
            {
                Console.WriteLine("Data trimisa este invalida");
            }
            Console.ReadLine();
            
        }
    }
}
